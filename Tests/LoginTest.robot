*** Settings ***
Library    SeleniumLibrary

Suite Setup           Log    Inside the Test Suite Setup
Suite Teardown        Log    Inside the Test Suite Teardown
Test Setup            Log    Inside the Test Case Setup
Test Teardown         Log    Inside the Test Suite Teardown   

Default Tags    sanity

*** Test Cases ***
FirstTestCase
    [Tags]    smoke
    Log    Inside FirstTestCase
SecondTestCase
    Set Tags    regression1
    Log    Inside SencondTestCase
ThirdTestCase
    Log    Inside ThirdTestCase    

# LoginToPolarion
    # Open Browser                 ${PolarionURL}    chrome
    # Set Browser Implicit Wait    4
    # Input Text                   name=j_username    ${PolarionCredentials}[0]
    # Input Password               name=j_password    ${PolarionCredentials}[1]
    # Click Button                 name=submit
    # Sleep                        3
    # Click Element                id=DOM_104
    # Sleep                        3
    # Close Browser
    # Log                          This test was executed by %{username} on %{os}
    
# LoginToOrangehrmlive
    # Open Browser                  ${OrangeURL}    chrome
    # Set Browser Implicit Wait     5
    # LoginOrange
    # Click Element                 id=welcome
    # Sleep                         5   
    # Click Link                    link=Logout
    # Close Browser 
    # Log                           This test was executed by %{username} on %{os}    
  
# *** Variables ***
# ${OrangeURL}                      https://opensource-demo.orangehrmlive.com   
# ${PolarionURL}                    http://pl1segot0002nb:8632/polarion
# @{OrangeCredentials}              Admin    admin123
# @{PolarionCredentials}            peter    peter
# &{LOGIN}                          password=admin123

# *** Keywords ***
# LoginOrange
    # Input Text                    id=txtUsername    ${OrangeCredentials}[0]
    # Input Password                id=txtPassword    &{LOGIN}[password]
    # Click Button                  id=btnLogin